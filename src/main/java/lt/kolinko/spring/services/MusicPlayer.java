package lt.kolinko.spring.services;

import lombok.Getter;
import lt.kolinko.spring.repositories.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Random;

// Spring creates bean using annotation @Component
@Component
@Getter
public class MusicPlayer {
    /*
    @Autowired : auto DI by Spring
    @Qualifier : pointer to bean need to be used
    @Value : values injection into variables and method arguments (String, int, double, etc.)
    ${"..."} : value injection via external .properties file
        To resolve ${"..."} in @Value must use @PropertySource
    @PostConstruct : equivalent of init-method
        init-method executes before getting bean from application context
    @PreDestroy : equivalent of destroy-method
        destroy-method executes when application context shuts down
    */

    private final List<Music> musicList;
    @Value("${musicPlayer.name}") private String name;
    @Value("${musicPlayer.volume}") private double volume;

    public MusicPlayer(List<Music> musicList) {
        this.musicList = musicList;
    }

    public void playMusic(Genre genre) {
        try {
            switch (genre) {
                case POP:
                    System.out.printf("Playing: %s, volume: %.02f%s\n", musicList.get(0).getSongs().get(new Random().nextInt(2)), volume, "%");
                    break;
                case TECHNO:
                    System.out.printf("Playing: %s, volume: %.02f%s\n", musicList.get(1).getSongs().get(new Random().nextInt(2)), volume, "%");
                    break;
                case RAVE:
                    System.out.printf("Playing: %s, volume: %.02f%s\n", musicList.get(2).getSongs().get(new Random().nextInt(2)), volume, "%");
                    break;
            }
        } catch (NullPointerException e) {
            System.err.println("Nothing to play");
        }
    }

    // init-method equivalent
    @PostConstruct
    public void init() {
        System.out.println("Init method is running here...");
    }

    // destroy-method equivalent
    @PreDestroy
    public void destroy() {
        System.out.println("Destroy method is running here...");
    }
}
