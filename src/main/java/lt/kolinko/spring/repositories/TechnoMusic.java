package lt.kolinko.spring.repositories;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

// Spring creates bean using annotation @Component
@Component
public class TechnoMusic implements Music {
    @Override
    public List<String> getSongs() {
        return Arrays.asList(
                "Westbam - Hard Times",
                "Tiesto - Loves Come Again",
                "The Prodigy - Breathe"
        );
    }
}
