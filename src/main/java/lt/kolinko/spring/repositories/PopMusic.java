package lt.kolinko.spring.repositories;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

// Spring creates bean using annotation @Component
@Component
public class PopMusic implements Music {
    @Override
    public List<String> getSongs() {
        return Arrays.asList(
                "Ariana Grande - No Tears Left To Cry",
                "Lady Gaga - Poker Face",
                "Backstreet Boys - Drowning"
        );
    }
}
