package lt.kolinko.spring.repositories;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

// Spring creates bean using annotation @Component
@Component
public class RaveMusic implements Music {
    @Override
    public List<String> getSongs() {
        return Arrays.asList(
                "Scooter - Faster Harder Scooter",
                "Marusha - Free Love",
                "Fatboy Slim - Right Here, Right Now"
        );
    }
}
