package lt.kolinko.spring.config;

import lt.kolinko.spring.repositories.PopMusic;
import lt.kolinko.spring.repositories.RaveMusic;
import lt.kolinko.spring.repositories.TechnoMusic;
import lt.kolinko.spring.services.MusicPlayer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Arrays;

/*
@Configuration indicates that a class declares one or more @Bean methods and may be processed by the Spring container
to generate bean definitions and service requests for those beans at runtime

@ComponentScan enables component (beans) scanning
    basePackages attribute specifies which packages should be scanned for decorated beans

@PropertySource is a convenient mechanism for adding property sources to the environment
    classpath: is a path to "resources" folder
 */

@Configuration
@ComponentScan(basePackages = "lt.kolinko.spring")
@PropertySource("classpath:properties/app.properties")
public class SpringConfig {
    @Bean
    public PopMusic popMusic() {
        return new PopMusic();
    }

    @Bean
    public TechnoMusic technoMusic() {
        return new TechnoMusic();
    }

    @Bean
    public RaveMusic raveMusic() {
        return new RaveMusic();
    }

    @Bean
    public MusicPlayer musicPlayer() {
        // immutable list
        return new MusicPlayer(Arrays.asList(popMusic(), technoMusic(), raveMusic()));
    }
}
